/* automatically generated from ./terminal-window-ui.xml */
#ifdef __SUNPRO_C
#pragma align 4 (terminal_window_ui)
#endif
#ifdef __GNUC__
static const char terminal_window_ui[] __attribute__ ((__aligned__ (4))) =
#else
static const char terminal_window_ui[] =
#endif
{
  "<ui><menubar name=\"main-menu\"><menu action=\"file-menu\"><menuitem ac"
  "tion=\"new-tab\"/><menuitem action=\"new-window\"/><menuitem action=\"u"
  "ndo-close-tab\"/><separator/><menuitem action=\"detach-tab\"/><separato"
  "r/><menuitem action=\"close-tab\"/><menuitem action=\"close-other-tabs\""
  "/><menuitem action=\"close-window\"/></menu><menu action=\"edit-menu\">"
  "<menuitem action=\"copy\"/><menuitem action=\"paste\"/><menuitem action"
  "=\"paste-selection\"/><separator/><menuitem action=\"select-all\"/><sep"
  "arator/><menuitem action=\"copy-input\"/><separator/><menuitem action=\""
  "preferences\"/></menu><menu action=\"view-menu\"><menuitem action=\"sho"
  "w-menubar\"/><menuitem action=\"show-toolbar\"/><menuitem action=\"show"
  "-borders\"/><menuitem action=\"fullscreen\"/><separator/><menuitem acti"
  "on=\"zoom-in\"/><menuitem action=\"zoom-out\"/><menuitem action=\"zoom-"
  "reset\"/></menu><menu action=\"terminal-menu\"><menuitem action=\"set-t"
  "itle\"/><menuitem action=\"set-title-color\"/><separator/><menuitem act"
  "ion=\"search\"/><menuitem action=\"search-next\"/><menuitem action=\"se"
  "arch-prev\"/><separator/><menuitem action=\"set-encoding\"/><separator/"
  "><menuitem action=\"read-only\"/><menuitem action=\"scroll-on-output\"/"
  "><separator/><menuitem action=\"save-contents\"/><separator/><menuitem "
  "action=\"reset\"/><menuitem action=\"reset-and-clear\"/></menu><menu ac"
  "tion=\"tabs-menu\"><menuitem action=\"prev-tab\"/><menuitem action=\"ne"
  "xt-tab\"/><menuitem action=\"last-active-tab\"/><separator/><menuitem a"
  "ction=\"move-tab-left\"/><menuitem action=\"move-tab-right\"/><separato"
  "r/><placeholder name=\"placeholder-tab-items\"/></menu><menu action=\"h"
  "elp-menu\"><menuitem action=\"contents\"/><menuitem action=\"about\"/><"
  "/menu></menubar><popup name=\"popup-menu\"><menuitem action=\"new-windo"
  "w\"/><menuitem action=\"new-tab\"/><separator/><menuitem action=\"copy\""
  "/><menuitem action=\"paste\"/><separator/><menuitem action=\"show-menub"
  "ar\"/><menuitem action=\"fullscreen\"/><menuitem action=\"read-only\"/>"
  "<separator/><menu action=\"zoom-menu\"><menuitem action=\"zoom-in\"/><m"
  "enuitem action=\"zoom-out\"/><menuitem action=\"zoom-reset\"/></menu><s"
  "eparator/><menuitem action=\"save-contents\"/><separator/><menuitem act"
  "ion=\"preferences\"/></popup><popup name=\"tab-menu\"><menuitem action="
  "\"detach-tab\"/><menuitem action=\"close-other-tabs\"/><separator/><men"
  "uitem action=\"set-title\"/><menuitem action=\"set-title-color\"/><sepa"
  "rator/><menuitem action=\"move-tab-left\"/><menuitem action=\"move-tab-"
  "right\"/><separator/><menu action=\"tabs-menu\"><placeholder name=\"pla"
  "ceholder-tab-items\"/></menu><separator/><menuitem action=\"close-tab\""
  "/></popup><toolbar name=\"main-toolbar\"><toolitem action=\"new-tab\"/>"
  "<toolitem action=\"new-window\"/><separator/><toolitem action=\"copy\"/"
  "><toolitem action=\"paste\"/><separator/><toolitem action=\"search\"/><"
  "separator/><toolitem action=\"fullscreen\"/><toolitem action=\"preferen"
  "ces\"/><separator/><toolitem action=\"prev-tab\"/><toolitem action=\"ne"
  "xt-tab\"/><separator/><toolitem action=\"contents\"/></toolbar></ui>"
};

static const unsigned terminal_window_ui_length = 2891u;

